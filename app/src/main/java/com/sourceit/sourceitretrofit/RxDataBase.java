package com.sourceit.sourceitretrofit;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by wenceslaus on 11.11.16.
 */

public class RxDataBase {

    public static Observable<Cursor> query(
            final ContentResolver cr,
            final Uri uri,
            final String[] projection,
            final String selection,
            final String[] selectionArgs,
            final String sortOrder) {
        return Observable.create(new Observable.OnSubscribe<Cursor>() {
            @Override
            public void call(Subscriber<? super Cursor> observer) {
                if (!observer.isUnsubscribed()) {
                    Cursor cursor = null;
                    try {
                        cursor = cr.query(uri, projection, selection, selectionArgs, sortOrder);
                        if (cursor != null && cursor.getCount() > 0) {
                            while (cursor.moveToNext()) {
                                observer.onNext(cursor);
                            }
                        }
                        observer.onCompleted();
                    } catch (Exception err) {
                        observer.onError(err);

                    } finally {
                        if (cursor != null) cursor.close();
                    }
                }
            }
        });
    }

}
